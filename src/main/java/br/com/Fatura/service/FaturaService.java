package br.com.Fatura.service;

import br.com.Fatura.clients.CartaoClient;
import br.com.Fatura.clients.PagamentoClient;
import br.com.Fatura.clients.dtoClient.CartaoDTO;
import br.com.Fatura.clients.dtoClient.PagamentoDTO;
import br.com.Fatura.exception.FaturaEmptyException;
import br.com.Fatura.models.Fatura;
import br.com.Fatura.models.dtos.CartaoExpiradoResponse;
import br.com.Fatura.repositories.FaturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class FaturaService {

    @Autowired
    private FaturaRepository faturaRepository;

    @Autowired
    private CartaoClient cartaoClient;

    @Autowired
    private PagamentoClient pagamentoClient;

    public Fatura pagarFatura(int clienteId, int cartaoId){

        /*
        1 - consulta se o cartao existe - Cartao - ok
        2 - consulta todos os pagamentos feitos pelo cartão - Pagamento - ok
        3 - grava a soma dos valores pagos em fatura - Fatura - ok
        4 - deleta os pagamentos feitos pelo cartao - Pagamento - TODO
         */

        Optional<CartaoDTO> cartaoOptional = cartaoClient.consultaCartao(cartaoId);

        Iterable<PagamentoDTO> pagamentos = getFatura(clienteId,cartaoId);
//        if(pagamentos.iterator().hasNext()) throw new FaturaEmptyException();
//        List<PagamentoDTO> pagamentos = pagamentoClient.buscaTodosPagamentosDoCartao(cartaoId);
//        if(pagamentos.isEmpty()) throw new FaturaEmptyException();

        double valorPago = 0.0;
        for (PagamentoDTO pagamento: pagamentos) {
            valorPago += pagamento.getValor();
        }
        Fatura fatura = new Fatura();
        fatura.setCartaoId(cartaoId);
        fatura.setPagoEm(LocalDate.now());
        fatura.setValorPago(valorPago);

        pagamentoClient.deletarPagamentos(cartaoId);

        return faturaRepository.save(fatura);
    }

    public Iterable<PagamentoDTO> getFatura(int clienteId, int cartaoId){
        List<PagamentoDTO> pagamentos = pagamentoClient.buscaTodosPagamentosDoCartao(cartaoId);
        if(pagamentos.isEmpty()) throw new FaturaEmptyException();
        return pagamentos;
    }

    public String expirarCartao(int clienteId, int cartaoId){
        Optional<CartaoDTO> cartaoDTOOptional = cartaoClient.expirarCartao(cartaoId);
        return "ok";
    }

}
