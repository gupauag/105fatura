package br.com.Fatura.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.FeignException;
import org.json.JSONException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ErrorHandler {

    private String mensagemPadrao = "Campos informados incorretamente no Body";

    @ExceptionHandler(FeignException.class)
    public ResponseEntity handleFeignException(FeignException e, HttpServletResponse response)
            throws JSONException, IOException {

        HashMap<String,Object> result =
                (HashMap<String, Object>) new ObjectMapper().readValue(e.contentUTF8(),Map.class);

        return ResponseEntity.status(e.status()).body(result);
    }

}
