package br.com.Fatura.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Fatura zerada. Não há pagamentos realizados neste cartão.")
public class FaturaEmptyException extends RuntimeException{
}
