package br.com.Fatura.repositories;

import br.com.Fatura.models.Fatura;
import org.springframework.data.repository.CrudRepository;

public interface FaturaRepository extends CrudRepository<Fatura,Integer> {
}
