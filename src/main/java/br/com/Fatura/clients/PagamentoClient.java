package br.com.Fatura.clients;

import br.com.Fatura.clients.dtoClient.PagamentoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "pagamento")
public interface PagamentoClient {

    @GetMapping("pagamentos/{cartaoId}")
//    List<PagamentoDTO> buscaTodosPagamentosDoCartao(int cartaoId);
    List<PagamentoDTO> buscaTodosPagamentosDoCartao(@PathVariable int cartaoId);

    @DeleteMapping("pagamentos/{cartaoId}")
    void deletarPagamentos(@PathVariable(name = "cartaoId") int cartaoId);

}
