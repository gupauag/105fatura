package br.com.Fatura.clients;

import br.com.Fatura.clients.dtoClient.CartaoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Optional;

@FeignClient(name = "cartao")
public interface CartaoClient {

    @GetMapping("/cartao/{id}")
    Optional<CartaoDTO> consultaCartao(@PathVariable int id);

    @PostMapping("/cartao/{id}/expirar")
    Optional<CartaoDTO> expirarCartao(@PathVariable int id);
}
