package br.com.Fatura.controller;

import br.com.Fatura.clients.dtoClient.PagamentoDTO;
import br.com.Fatura.models.Fatura;
import br.com.Fatura.models.dtos.CartaoExpiradoResponse;
import br.com.Fatura.service.FaturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/fatura")
@RestController
public class FaturaController {

    @Autowired
    private FaturaService faturaService;

    @PostMapping("/{cliente-id}/{cartao-id}/pagar")
    public ResponseEntity<Fatura> pagarFatura(@PathVariable(name = "cliente-id") int clienteId,
                                              @PathVariable(name = "cartao-id") int cartaoId) {
        return ResponseEntity.status(200).body(faturaService.pagarFatura(clienteId,cartaoId));
    }

    @GetMapping("/{cliente-id}/{cartao-id}")
    public ResponseEntity<Iterable<PagamentoDTO>> getFatura(@PathVariable(name = "cliente-id") int clienteId,
                                                  @PathVariable(name = "cartao-id") int cartaoId) {
        return ResponseEntity.status(200).body(faturaService.getFatura(clienteId,cartaoId));
    }

    @PostMapping("/{cliente-id}/{cartao-id}/expirar")
    public ResponseEntity<CartaoExpiradoResponse> expirarCartao(@PathVariable(name = "cliente-id") int clienteId,
                                                               @PathVariable(name = "cartao-id") int cartaoId) {
        CartaoExpiradoResponse cartaoExpiradoResponse = new CartaoExpiradoResponse();
        cartaoExpiradoResponse.setStatus(faturaService.expirarCartao(clienteId,cartaoId));

        return ResponseEntity.status(200).body(cartaoExpiradoResponse);
    }

}
